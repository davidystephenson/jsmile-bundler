(function(f){if(typeof exports==="object"&&typeof module!=="undefined"){module.exports=f()}else if(typeof define==="function"&&define.amd){define([],f)}else{var g;if(typeof window!=="undefined"){g=window}else if(typeof global!=="undefined"){g=global}else if(typeof self!=="undefined"){g=self}else{g=this}g.jsmile = f()}})(function(){var define,module,exports;return (function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
const isFunction = value => value && {}.toString.call(value) === '[object Function]'

const views = { "atomic/atoms/heading": require('/home/david/Sync/jsmile-bundler/test/views/atomic/atoms/heading.js'), "block": require('/home/david/Sync/jsmile-bundler/test/views/block.js'), "built": require('/home/david/Sync/jsmile-bundler/test/views/built.js'), "dependers/need": require('/home/david/Sync/jsmile-bundler/test/views/dependers/need.js'), "direct": require('/home/david/Sync/jsmile-bundler/test/views/direct.js'), "flag": require('/home/david/Sync/jsmile-bundler/test/views/flag.js'), "matched": require('/home/david/Sync/jsmile-bundler/test/views/matched.js'), "need": require('/home/david/Sync/jsmile-bundler/test/views/need.js'), "needer": require('/home/david/Sync/jsmile-bundler/test/views/needer.js'), "notNeeded": require('/home/david/Sync/jsmile-bundler/test/views/notNeeded.js'), "passer": require('/home/david/Sync/jsmile-bundler/test/views/passer.js'), "title": require('/home/david/Sync/jsmile-bundler/test/views/title.js') }

const includersFactory = require('/home/david/Sync/jsmile-bundler/test/includers.js')

const library = {
  build: require('jsml-davidystephenson'),
  browser: require('jsmile-browser'),
  views,
}

if (typeof window !== 'undefined' && typeof window.document !== 'undefined') {
  library.render = library.browser(window)
  window.jsml = library.render

  const includers = includersFactory(library, window)
  library.includers = includers

  const make = (view, options) => view
    ? view(library, options)
    : null

  const include = (name, options = {}) => {
    const view = views[name]
    const made = make(view, options)

    const includer = includers[name]
    if (includer) {
      return includer(made)
    } else {
      return made
    }
  }

  library.include = include

  library.depended = []
  library.depend = (name, options = {}) => {
    if (library.depended.indexOf(name) === -1) {
      include(name, options)

      library.depended.push(name)
    }

    return null
  }
}

module.exports = library
},{"/home/david/Sync/jsmile-bundler/test/includers.js":4,"/home/david/Sync/jsmile-bundler/test/views/atomic/atoms/heading.js":5,"/home/david/Sync/jsmile-bundler/test/views/block.js":6,"/home/david/Sync/jsmile-bundler/test/views/built.js":7,"/home/david/Sync/jsmile-bundler/test/views/dependers/need.js":8,"/home/david/Sync/jsmile-bundler/test/views/direct.js":9,"/home/david/Sync/jsmile-bundler/test/views/flag.js":10,"/home/david/Sync/jsmile-bundler/test/views/matched.js":11,"/home/david/Sync/jsmile-bundler/test/views/need.js":12,"/home/david/Sync/jsmile-bundler/test/views/needer.js":13,"/home/david/Sync/jsmile-bundler/test/views/notNeeded.js":14,"/home/david/Sync/jsmile-bundler/test/views/passer.js":15,"/home/david/Sync/jsmile-bundler/test/views/title.js":16,"jsmile-browser":2,"jsml-davidystephenson":3}],2:[function(require,module,exports){
const jsml = require('jsml-davidystephenson')

const is = value => value
const isString = value => typeof value === 'string'
const isNumber = value => !isNaN(value) && typeof value === 'number'
const isAnyObject = value => value === Object(value)
// const isValid = value => isString(value) || isNumber(value) || isAnyObject(value)

const isArray = value => Array.isArray(value)
const isFunction = value => value && {}.toString.call(value) === '[object Function]'
const isObject = value => !Array.isArray(value) &&
  !isFunction(value) &&
  isAnyObject(value)

// const isElement = value => typeof Element === 'object'
//   ? value instanceof Element
//   : value &&
//     typeof value === 'object' &&
//     value !== null &&
//     value.nodeType === 1 &&
//     typeof value.nodeName === 'string'

const strip = (data, keys) => {
  const clone = Object.assign({}, data)
  const values = {}

  if (clone) {
    keys.map(key => {
      const value = clone[key]

      if (value) {
        values[key] = value
        clone[key] = null
      }
    })
  }

  return [clone, values]
}

const enrich = (element, data, enricher) => {
  if (data) {
    Object.entries(data).map(entry => {
      const key = entry[0]
      const value = entry[1]

      enricher(element, key, value)
    })
  }
}

const listen = (element, key, value) => element.addEventListener(key, value)

const renderer = window => {
  const isNode = value => typeof window.Node === 'object'
    ? value instanceof window.Node
    : value &&
      typeof value === 'object' &&
      typeof value.nodeType === 'number' &&
      typeof value.nodeName === 'string'
  const isJquery = value => window.jQuery && value instanceof window.jQuery

  const build = data => {
    const [stripped, values] = strip(data, ['child', 'on'])

    const html = jsml(stripped)

    const template = window.document.createElement('template')
    template.innerHTML = html

    const element = template.content.firstChild

    enrich(element, values.on, listen)
    append(element, values.child)

    return element
  }

  const append = (a, b) => {
    if (a && b) {
      if (isJquery(a) || isNode(a)) {
        if (isJquery(b)) {
          b.each((index, element) => a.append(element))
        } else if (isNode(b)) {
          a.append(b)
        } else {
          const c = make(b)

          append(a, c)
        }
      } else {
        const d = make(a)

        append(d, b)
      }
    }
  }

  const make = data => {
    if (isNode(data) || isJquery(data)) {
      return data
    } else if (isArray(data)) {
      const fragment = window.document.createDocumentFragment()

      data
        .map(make)
        .map(node => append(fragment, node))

      return fragment
    } else if (isFunction(data)) {
      return make(data())
    } else if (isObject(data)) {
      return build(data)
    } else if (isString(data)) {
      return window.document.createTextNode(data)
    } else if (isNumber(data)) {
      return make(data.toString())
    } else {
      return null
    }
  }

  return (data, ...children) => {
    const base = make(data)

    children
      .map(make)
      .filter(is)
      .map(child => append(base, child))

    return base
  }
}

if (typeof window !== 'undefined' && typeof window.document !== 'undefined') {
  window.jsml = renderer(window)
}

module.exports = renderer

},{"jsml-davidystephenson":3}],3:[function(require,module,exports){
const SELF_CLOSING_TAGS = ['area', 'base', 'br', 'col', 'command', 'embed', 'hr', 'img', 'input', 'keygen', 'link', 'meta', 'param', 'source', 'track', 'wbr'];

const isSelfClosing = tag => SELF_CLOSING_TAGS.indexOf(tag) > -1;

const is = value => value;

const isString = value => typeof value === 'string';

const isNumber = value => !isNaN(value) && typeof value === 'number';

const isAnyObject = value => value === Object(value);

const isValid = value => isString(value) || isNumber(value) || isAnyObject(value);

const isArray = value => Array.isArray(value);

const isFunction = value => value && {}.toString.call(value) === '[object Function]';

const isObject = value => !Array.isArray(value) && !isFunction(value) && isAnyObject(value);

const buildProperties = properties => properties.map(entry => buildProperty(entry)).filter(is).join(' ');

const buildTag = element => {
  element.tag = `<${element.data.tag}`; // Attach the the data properties as HTML attributes.

  if (element.data.className) {
    element.data.class = element.data.className;
  }

  const filter = property => entry => entry[0] !== property;

  element.properties = Object.entries(element.data).filter(filter('tag')).filter(filter('className'));
  element.properties = element.selfClosing ? element.properties : element.properties.filter(filter('child'));

  if (element.properties.length) {
    const properties = buildProperties(element.properties);

    if (properties.length) {
      element.tag += ` ${properties}`;
    }
  }

  element.tag += '>';
  return element.tag;
};

const formatProperty = (name, property) => `${name}="${property}"`;

const parse = (value, delimiter = '') => {
  if (isNumber(value)) {
    return value.toString();
  } else if (isFunction(value)) {
    return build(value(), {
      delimiter
    });
  } else if (isString(value)) {
    return value;
  } else if (isArray(value)) {
    const result = value.filter(isValid).map(build).filter(isValid).join(delimiter);
    return result;
  } else {
    return null;
  }
};

const buildProperty = entry => {
  const name = entry[0];
  const value = entry[1];

  if (value === '' || value === true) {
    return name;
  } else {
    const parsed = parse(value, ' ');

    if (parsed) {
      return formatProperty(name, parsed.toString());
    } else if (isObject(value)) {
      const entries = Object.entries(value).map(entry => [`${name}-${entry[0]}`, entry[1]]);
      return buildProperties(entries);
    } else {
      return null;
    }
  }
};

const build = (data, options) => {
  const delimiter = options && options.delimiter ? options.delimiter : '';
  const result = parse(data, delimiter);

  if (result) {
    return result;
  } else if (isObject(data)) {
    if (!data.tag) {
      data.tag = 'div';
    }

    const element = {
      data
    };
    element.selfClosing = isSelfClosing(data.tag);
    let output = buildTag(element);

    if (!element.selfClosing) {
      if (data.child) {
        output += build(data.child);
      }

      output += `</${data.tag}>`;
    }

    return output;
  }
};

module.exports = build;

},{}],4:[function(require,module,exports){
module.exports = (jsmile, window) => {
  const body = window.document.querySelector('body')

  return {
    built (built) {
      return Object.assign({}, built, { included: true })
    },

    count () {
      window.count++
    },

    flag (flag) {
      window.flag = flag
    },

    matched (view) {
      return Object.assign({}, view, { matched: true })
    },

    need (need) {
      const note = { class: 'note', child: `I needed a ${need.tag}.` }

      jsmile.render(body, need, note)
    },

    nothing (output) {
      jsmile.render(body, { class: 'nothing', child: `${output === null}` })
    },

    passer (options) {
      window.passed = options
    },

    unmatched () {
      return 'The Spanish Inquisition'
    }
  }
}

},{}],5:[function(require,module,exports){
module.exports = (jsmile, options) => ({
  class: 'heading',
  tag: 'h2',
  child: options.text
})

},{}],6:[function(require,module,exports){
module.exports = (jsmile, options) => ({
  class: 'block',
  child: [
    jsmile.include('title', { text: options.title }),
    { class: 'content', child: options.content }
  ]
})

},{}],7:[function(require,module,exports){
module.exports = (jsmile, options) => Object.assign({}, options, { built: true })

},{}],8:[function(require,module,exports){
module.exports = (window, dependency) => window
  .document
  .querySelector('body')
  .append(dependency)

},{}],9:[function(require,module,exports){
module.exports = (jsmile, options) => options

},{}],10:[function(require,module,exports){
module.exports = () => 'flag'

},{}],11:[function(require,module,exports){
module.exports = () => ({ class: 'matched' })

},{}],12:[function(require,module,exports){
module.exports = (jsmile, options) => ({
  tag: options.tag || 'p',
  class: 'needing',
  child: "I'm needed."
})

},{}],13:[function(require,module,exports){
module.exports = (jsmile, options) => [
  jsmile.depend('need', { tag: 'span' }),
  { class: options.class || 'needer', child: 'I need something.' }
]

},{}],14:[function(require,module,exports){
module.exports = jsmile => jsmile.depend('need')

},{}],15:[function(require,module,exports){
arguments[4][9][0].apply(exports,arguments)
},{"dup":9}],16:[function(require,module,exports){
module.exports = (jsmile, options = {}) => ({
  class: 'title',
  tag: 'h1',
  child: options.text
})

},{}]},{},[1])(1)
});
