module.exports = (jsmile, window) => {
  const body = window.document.querySelector('body')

  return {
    built (built) {
      return Object.assign({}, built, { included: true })
    },

    count () {
      window.count++
    },

    flag (flag) {
      window.flag = flag
    },

    matched (view) {
      return Object.assign({}, view, { matched: true })
    },

    need (need) {
      const note = { class: 'note', child: `I needed a ${need.tag}.` }

      jsmile.render(body, need, note)
    },

    nothing (output) {
      jsmile.render(body, { class: 'nothing', child: `${output === null}` })
    },

    passer (options) {
      window.passed = options
    },

    unmatched () {
      return 'The Spanish Inquisition'
    }
  }
}
