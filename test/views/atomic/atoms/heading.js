module.exports = (jsmile, options) => ({
  class: 'heading',
  tag: 'h2',
  child: options.text
})
