module.exports = (jsmile, options) => [
  jsmile.depend('need', { tag: 'span' }),
  { class: options.class || 'needer', child: 'I need something.' }
]
