module.exports = (jsmile, options) => ({
  class: 'block',
  child: [
    jsmile.include('title', { text: options.title }),
    { class: 'content', child: options.content }
  ]
})
