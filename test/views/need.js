module.exports = (jsmile, options) => ({
  tag: options.tag || 'p',
  class: 'needing',
  child: "I'm needed."
})
