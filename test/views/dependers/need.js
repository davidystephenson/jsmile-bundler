module.exports = (window, dependency) => window
  .document
  .querySelector('body')
  .append(dependency)
