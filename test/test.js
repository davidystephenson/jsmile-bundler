/* global describe, it, beforeEach  */

const assert = require('assert')
const jsdom = require('jsdom')

;(() => {
  describe('JSMiLe-bundler', () => {
    let window

    beforeEach(() => {
      const virtualConsole = new jsdom.VirtualConsole()
      virtualConsole.sendTo(console)

      return jsdom
        .JSDOM
        .fromFile(
          'test/test.html',
          {
            resources: 'usable',
            runScripts: 'dangerously',
            virtualConsole
          }
        )
        .then((dom) => { window = dom.window })
    })

    const loader = checker => done => window
      .addEventListener('load', () => {
        checker()
        done()
      })

    describe('render', () => {
      it('is automatically available in the browser', loader(() => {
        assert(window.jsmile.render)
        assert.deepStrictEqual(
          window.jsmile.render({}),
          window.document.createElement('div')
        )

        const greeting = window.document.querySelector('div').textContent
        assert.strictEqual(greeting, 'hello world')
      }))

      it('is automatically available in the browser', loader(() => {
        assert(window.jsmile.render)
        assert.deepStrictEqual(
          window.jsmile.render({}),
          window.document.createElement('div')
        )

        const greeting = window.document.querySelector('div').textContent
        assert.strictEqual(greeting, 'hello world')
      }))

      describe('jsml', () => {
        it('is not automatically available in node', () => {
          assert.strictEqual(global.jsml, undefined)
        })

        it('is automatically provided in the browser', loader(() => {
          assert(window.jsml)
          assert.deepStrictEqual(
            window.jsml({}),
            window.document.createElement('div')
          )

          const exclamation = window.document.querySelector('span').textContent
          assert.strictEqual(exclamation, '!')
        }))
      })

      describe('jQuery', done => {
        it('appends to all elements in a collection', loader(() => {
          const items = window.document.querySelectorAll('li')

          for (const item of items) {
            assert.strictEqual(item.textContent, 'something!')
          }
        }))

        it('appends all elements in a collection', loader(() => {
          const list = window.document.querySelector('ul')

          assert.strictEqual(list.childNodes.length, 3)

          list.childNodes.forEach(node => {
            assert.strictEqual(node.tagName, 'LI')
            assert.strictEqual(node.textContent, 'something!')
          })
        }))
      })
    })

    describe('views', () => {
      it('are exposed', loader(() => {
        assert(window.jsmile.views.title)
        const title = require('./views/title')
        assert.strictEqual(window.jsmile.views.title.toString(), title.toString())
      }))
    })

    describe('includers', () => {
      it('are exposed', loader(() => assert(window.jsmile.includers.need)))
    })

    describe('include', () => {
      it('returns null for non-existent views', loader(() => {
        try {
          const i = require('./views/squareRootOf1')
          assert.strictEqual(i, undefined)
        } catch (error) {
          assert.strictEqual(error.message, "Cannot find module './views/squareRootOf1'")
        }

        assert.strictEqual(window.jsmile.include('squareRootOf1'), null)
      }))

      it('passes an empty options object by default', loader(() => {
        const options = window.jsmile.include('direct')

        assert.deepEqual(options, {})
      }))

      it('supports nested includes', loader(() => {
        const block = window.jsmile.include('block', { title: 'title' })
        assert.deepEqual(block.child[0], { class: 'title', tag: 'h1', child: 'title' })

        const title = window.document.querySelector('.title')
        assert.strictEqual(title.textContent, 'title')
      }))

      it('supports subdirectory view lookup', loader(() => {
        const atom = require('./views/atomic/atoms/heading')
        const view = window.jsmile.views['atomic/atoms/heading']
        assert.strictEqual(atom.toString(), view.toString())

        const heading = window.document.querySelector('.heading')
        assert.strictEqual(heading.textContent, 'heading')
      }))

      describe('runs matching includers', () => {
        it('for defined views', loader(() => {
          const matched = window.jsmile.include('matched')
          assert.deepEqual(matched, { matched: true, class: 'matched' })

          const note = window.document.querySelector('.note')
          assert.strictEqual(note.tagName, 'DIV')
        }))

        it('for undefined views', loader(() => {
          const unmatched = window.jsmile.include('unmatched')
          assert.strictEqual(unmatched, 'The Spanish Inquisition')

          window.jsmile.include('nothing')
          const nothing = window.document.querySelector('.nothing')
          assert.strictEqual(nothing.textContent, 'true')
        }))

        it('with built view data', loader(() => {
          assert(window.jsmile.include('built').built)

          const note = window.document.querySelector('.note')
          assert.strictEqual(note.textContent, 'I needed a span.')
        }))

        it("and returns the includer's return value", loader(() => {
          assert(window.jsmile.include('built').included)
        }))
      })
    })

    describe('depended', () => {
      it('exposes dependencies', loader(() => {
        assert.deepEqual(window.jsmile.depended, ['need'])

        window.jsmile.depend('after')
        assert.deepEqual(window.jsmile.depended, ['need', 'after'])
      }))
    })

    describe('depend', () => {
      it('returns null', loader(() => {
        assert.strictEqual(window.jsmile.depend('anything'), null)

        assert.strictEqual(window.notNeeded, null)
      }))

      it('builds matching views and passes them to includers', loader(() => {
        assert.strictEqual(window.flag, undefined)
        window.jsmile.depend('flag')
        assert.strictEqual(window.flag, 'flag')

        const needer = window.document.querySelector('.needer')
        assert.strictEqual(needer.classList.value, 'needer')
      }))

      it('only runs includers once per view', loader(() => {
        window.count = 0
        window.jsmile.depend('count')
        assert.strictEqual(window.count, 1)
        window.jsmile.depend('count')
        assert.strictEqual(window.count, 1)

        const needers = window.document.querySelectorAll('.needer')
        assert.strictEqual(needers.length, 2)
        const needings = window.document.querySelectorAll('.needing')
        assert.strictEqual(needings.length, 1)
        const notes = window.document.querySelectorAll('.note')
        assert.strictEqual(notes.length, 1)
      }))

      it('passes options to views', loader(() => {
        const options = { key: 'value' }
        assert.strictEqual(window.passed, undefined)
        window.jsmile.depend('passer', options)
        assert.deepStrictEqual(window.passed, options)

        const needing = window.document.querySelector('.needing')
        assert.strictEqual(needing.tagName, 'SPAN')
      }))
    })
  })
})()
