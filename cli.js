#!/usr/bin/env node

const browserify = require('browserify')
const fs = require('fs')
const path = require('path')
const glob = require('glob')

const locate = file => path.join(process.cwd(), file)
const viewsPath = locate(process.argv[2])
const includerPath = locate(process.argv[3])
const publicPath = locate(process.argv[4])

const query = path.join(viewsPath, '**/*.js')
const requires = glob
  .sync(query)
  .map(view => {
    const relative = path.relative(viewsPath, view)
    const parsed = path.parse(relative)
    const joined = path.join(parsed.dir, parsed.name)

    return `"${joined}": require('${view}')`
  })

const client = `const isFunction = value => value && {}.toString.call(value) === '[object Function]'

const views = { ${requires.join(', ')} }

const includersFactory = require('${includerPath}')

const library = {
  build: require('jsml-davidystephenson'),
  browser: require('jsmile-browser'),
  views,
}

if (typeof window !== 'undefined' && typeof window.document !== 'undefined') {
  library.render = library.browser(window)
  window.jsml = library.render

  const includers = includersFactory(library, window)
  library.includers = includers

  const make = (view, options) => view
    ? view(library, options)
    : null

  const include = (name, options = {}) => {
    const view = views[name]
    const made = make(view, options)

    const includer = includers[name]
    if (includer) {
      return includer(made)
    } else {
      return made
    }
  }

  library.include = include

  library.depended = []
  library.depend = (name, options = {}) => {
    if (library.depended.indexOf(name) === -1) {
      include(name, options)

      library.depended.push(name)
    }

    return null
  }
}

module.exports = library`

const buildPath = './.__jsmile-library.js'
fs.writeFile(buildPath, client, error => {
  if (error) throw error

  const b = browserify({ standalone: 'jsmile' })

  b.add(buildPath)

  const output = fs.createWriteStream(publicPath)
  b
    .bundle()
    .pipe(output)
    .on(
      'finish',
      () => fs.unlink(
        buildPath,
        error => {
          if (error) throw error

          console.log('JSMiLe bundling finished!')
        }
      )
    )
})
